//
//  ViewController.swift
//  Calculator
//
//  Created by Soeng Saravit on 10/25/17.
//  Copyright © 2017 Soeng Saravit. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var resultSreen: UILabel!
    @IBOutlet weak var displayScreen: UILabel!
    
    var firstValueString: String = ""
    var currentValueString: String = ""
    var operatorSymbol = ""
    
    var isFirstValueTrue = true
    var isOperatorPressed = true
    var isEqualPress = false
    
    let formatter = NumberFormatter()
    
    @IBAction func inputNumber(_ sender: UIButton) {
        let inputNumber = sender.title(for: .normal)!
        
        //check .
        if displayScreen.text! == "0" && inputNumber == "." {
//            print("\(inputNumber)")
            currentValueString = "0."
        }

        //check if already contains .
        if inputNumber == "." && currentValueString.contains(".") {
            displayScreen.text = currentValueString
        } else{
            // set currentValueString to "" after clicking = operator, therefore currentString can contain new input number
            print(isFirstValueTrue)
            if !isFirstValueTrue {
                if inputNumber == "." {
                    currentValueString = "0"
                    resultSreen.text = ""
                }else {
                    currentValueString = ""
                    resultSreen.text = ""
                }
                    isFirstValueTrue = true
                }
                currentValueString += inputNumber
                displayScreen.text = currentValueString
            }
    }
    
    @IBAction func inputOperator(_ sender: UIButton) {
        let operatorInput = sender.title(for: .normal)!

        //press + - * / operator
        if operatorInput != "C" && operatorInput != "+/-" && operatorInput != "=" && displayScreen.text! != "Inf" {
            //will set number on displayScreen to firstValueString when get result and operator clicked
            print("\(currentValueString) \(firstValueString)")
            if isFirstValueTrue && !isEqualPress {
                firstValueString = displayScreen.text!
            }
 
            operatorSymbol = operatorInput

            resultSreen.text = firstValueString + " " + operatorInput
            if displayScreen.text! != "Inf" {
                displayScreen.text = "0"
            }
            currentValueString = ""
            
            isEqualPress = true
            isOperatorPressed = false
            isFirstValueTrue = true
        }
        
        
        //press equal operator
        if operatorInput == "=" {
            if displayScreen.text == "0" && operatorSymbol == "÷" {
                resultSreen.text = ""
                displayScreen.text = "Inf"
                isEqualPress = false
                isFirstValueTrue = true
            }else if isEqualPress {
                currentValueString = displayScreen.text!
                let firstValue = Double(firstValueString)!
                let currentValue = Double(currentValueString)!
                let result = calculate(firstValue, currentValue, operatorSymbol)
                 resultSreen.text = firstValueString + " " + operatorSymbol + " " +  currentValueString

                 displayScreen.text = "\(formatter.string(from: result as NSNumber) ?? "n/a")"
                 currentValueString = displayScreen.text!
                 
                 isFirstValueTrue = false
                 isOperatorPressed = false
                 isEqualPress = false
                
                firstValueString = displayScreen.text!
            }
        }
        
        //clear button clicked
        if operatorInput == "C" {
            displayScreen.text = "0"
            resultSreen.text = ""
            firstValueString = ""
            currentValueString = ""
            operatorSymbol = "+"
            isOperatorPressed = true
            isFirstValueTrue = true
            isEqualPress = false
        }
        
        if operatorInput == "+/-" && isEqualPress {
            if displayScreen.text != "0" {
                let currentValue = negativeValue(value: Double(displayScreen.text!)!)
                displayScreen.text = "\(formatter.string(from: currentValue as NSNumber) ?? "n/a")"
                print(negativeValue(value: currentValue))
            }
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        formatter.minimumFractionDigits = 0
        formatter.maximumFractionDigits = 5
    }
    
    func calculate(_ firstValue: Double, _ currentValue: Double, _ operatorSymbol: String) -> Double {
                switch operatorSymbol {
        case "+":
            return firstValue + currentValue
        case "-":
            return firstValue - currentValue
        case "×":
            return firstValue * currentValue
        case "÷":
            return firstValue / currentValue
        case "%":
            return firstValue.truncatingRemainder(dividingBy: currentValue)
        default:
            return 0.0
        }
    }
    
    func negativeValue(value: Double) -> Double{
        return value * (-1)
    }
}

@IBDesignable class customButton: UIButton {
    
    @IBInspectable
    public var cornerRadius: CGFloat = 0.0 {
        didSet {
           self.layer.cornerRadius = self.cornerRadius
        }
    }
}
